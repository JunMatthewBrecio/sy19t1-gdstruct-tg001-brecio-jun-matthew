#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "time.h"

using namespace std;

void main()
{
	srand(time(NULL));
	int userSize;
	int removeElement;
	int searchElement;
	int value;
	string checker;
	int choice;

	cout << "Input size: ";
	cin >> userSize;
	system("CLS");

	UnorderedArray<int> numbers(userSize);
	for (int i = 0; i < userSize; i++)
	{
		int numGenerator = rand() % 100 + 1;
		numbers.push(numGenerator);
	}

	while (1)
	{
		cout << "[1] Print Elements" << endl;
		cout << "[2] Remove an Element" << endl;
		cout << "[3] Find a Number" << endl;
		cin >> choice;
		system("CLS");

		if (choice == 1)
		{
			cout << "Elements:" << endl;
			for (int i = 0; i < numbers.getSize(); i++)
				cout << "#" << i + 1 << ": " << numbers[i] << endl;
		}

		if (choice == 2)
		{
			cout << "Remove Element #: ";
			cin >> removeElement;

			numbers.remove(removeElement - 1);
		}

		if (choice == 3)
		{
			cout << "Look for: ";
			cin >> searchElement;

			int searchedElement = numbers.linearSearch(searchElement, value, checker);

			if (searchedElement != -1)
			{
				cout << endl << "The value you enetered is the #" << searchedElement + 1 << " element!" << endl;
			}
			else
			{
				cout << endl << "Value not found." << endl;
			}
		}
		system("pause");
		system("CLS");
	}
}