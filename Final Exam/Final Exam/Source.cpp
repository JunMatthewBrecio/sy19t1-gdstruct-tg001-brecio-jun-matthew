#include <iostream>
#include <string>
#include "Queue.h"
#include "Stack.h"

using namespace std;

int main()
{
	cout << "Enter size for element sets: ";
	int size;
	cin >> size;

	Queue<int> Queue(size);
	Stack<int> Stack(size);

	while (1)
	{
		int number;
		system("CLS");

		cout << "What do you want to do?" << endl;
		cout << "1 - Push elements" << endl;
		cout << "2 - Pop elements" << endl;
		cout << "3 - Print everything then empty set" << endl;
		int choice;
		cin >> choice;
		system("CLS");

		if (choice == 1)
		{
			cout << "Enter number: ";
			cin >> number;
			Queue.push(number);
			Stack.push(number);

			system("CLS");
			cout << "Top element of sets:" << endl;
			cout << "Queue: " << Queue[0] << endl;
			cout << "Stack: " << Stack[0] << endl;
			system("pause");
		}

		if (choice == 2)
		{
			Queue.pop();
			Stack.pop();

			if(Queue.getSize() > 0)
			{
				cout << "You have popped the front elements." << endl;
				system("pause");

				system("CLS");
				cout << "Top element of sets:" << endl;
				cout << "Queue: " << Queue[0] << endl;
				cout << "Stack: " << Stack[0] << endl;

				system("pause");
				system("CLS");
			}
			else
			{
				cout << "You have popped the front elements." << endl;
				system("pause");
			}
		}

		if (choice == 3)
		{
			cout << "Top element of sets:" << endl;
			cout << "Queue: ";
			for (int i = 0; i < Queue.getSize(); i++)
			{
				cout << Queue[i] << " ";
			}
			cout << endl;

			cout << "Stack: ";
			for (int i = 0; i < Stack.getSize(); i++)
			{
				cout << Stack[i] << " ";
			}
			cout << endl;

			system("pause");

			Queue.remove();
			Stack.remove();
		}
	}
}