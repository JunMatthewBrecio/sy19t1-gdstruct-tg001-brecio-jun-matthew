#include <string>
#include <iostream>

using namespace std;

int main()
{
	string guildName;
	int guildNumber;
	string username;
	int renameMember;
	string newName;
	string newMember;
	int choice;
	int deleteMember;

	cout << "Input Guild Name: ";
	cin >> guildName;

	cout << "Input guild size: ";
	cin >> guildNumber;

	string *members = new string[guildNumber];
	for (int i = 0; i < guildNumber; i++)
	{
		cout << "Input username of user " << i + 1 << ":" << endl;
		cin >> username;
		members[i] = username;
	}

	system("CLS");

	while (1)
	{
		cout << "[1] Print Members" << endl;
		cout << "[2] Rename Member" << endl;
		cout << "[3] Add Members" << endl;
		cout << "[4] Delete Member" << endl;
		cin >> choice;

		system("CLS");

		if (choice == 1)
		{
			cout << guildName << " Members:" << endl;
			for (int i = 0; i < guildNumber; i++)
			{
				cout << members[i] << endl;
			}
		}

		if (choice == 2)
		{
			cout << "Rename Member #: ";
			cin >> renameMember;
			cout << "Input new name: ";
			cin >> newName;
			members[renameMember - 1] = newName;
		}

		if (choice == 3)
		{
			cout << "Adding a new member..." << endl;

			string *tempMembers = new string[guildNumber];
			for (int i = 0; i < guildNumber; i++)
			{
				tempMembers[i] = members[i];
			}
			delete[] members;
			guildNumber += 1;
			members = new string[guildNumber];
			for (int i = 0; i < (guildNumber - 1); i++)
			{
				members[i] = tempMembers[i];
			}

			cout << "Input username of new member: ";
			cin >> newMember;
			members[guildNumber - 1] = newMember;
		}

		if (choice == 4)
		{
			cout << "Delete Member #: ";
			cin >> deleteMember;
			members[deleteMember - 1] = "Deleted";
			
			string *tempMembers = new string[guildNumber];
			for (int i = 0; i < guildNumber; i++)
			{
				if (members[i] == "Deleted")
				{
					tempMembers[guildNumber-1] = members[i];
				}
				else
				{
					tempMembers[i] = members[i];
				}
			}
			delete[] members;
			/*guildNumber -= 1;*/
			members = new string[guildNumber];
			for (int i = 0; i < guildNumber; i++)
			{
				members[i] = tempMembers[i];
			}
		}

		system("pause");
		system("CLS");
	}
	return 0;
}