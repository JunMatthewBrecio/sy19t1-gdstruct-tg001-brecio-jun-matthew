#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	srand(time(NULL));
	cout << "Enter size for dynamic arrays: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	while(1)
	{
		system("CLS");
		cout << "Unordered contents: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << " ";
		cout << "\nOrdered contents: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << " ";

		cout << endl << endl;

		cout << "What do you want to do?" << endl;
		cout << "1 - Remove element at index" << endl;
		cout << "2 - Search for element" << endl;
		cout << "3 - Expand and generate random values" << endl;
		int choice;
		cin >> choice;

		if (choice == 1)
		{
			cout << "\n\nEnter index to remove: ";
			int removingIndex;
			cin >> removingIndex;

			while (removingIndex > (size-1))
			{
				cout << "Index is more than the current size! Enter another index: ";
				cin >> removingIndex;
			}

			unordered.remove(removingIndex);
			ordered.remove(removingIndex);

			cout << "\nElement removed: " << removingIndex << endl;
			cout << "Unordered contents: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << " ";
			cout << "\nOrdered contents: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << " ";
			cout << endl;
			system("pause");

		}
		if (choice == 2)
		{
			cout << "\nInput element to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << endl;
			system("pause");
		}
		if (choice == 3)
		{
			cout << "Input size of expansion: ";
			cin >> size;
			for (int i = 0; i < size; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
		}
	}
	system("pause");
}