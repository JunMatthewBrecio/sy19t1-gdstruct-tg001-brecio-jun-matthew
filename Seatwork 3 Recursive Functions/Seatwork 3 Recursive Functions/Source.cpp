#include <iostream>
#include <string>

using namespace std;

int digits(int input, int sum)
{
	if (input <= 0)
	{
		return sum;
	}
	sum = sum + input % 10;
	input = input / 10;
	digits(input, sum);
	;
}

int fibonacci(int input, int firstNum, int secNum)
{
	cout << firstNum << ", ";
	if (input == 0)
	{
		int num = firstNum + secNum;
		return num;
	}
	else
	{
		fibonacci(input - 1, secNum, firstNum + secNum);
	}
}

int main()
{
	int input;
	int sum = 0;
	int firstNum = 0;
	int secNum = 0;

	cout << "Sum of Digits" << endl << "Input number: "; // Sum of Digits
	cin >> input;

	cout << digits(input, sum) << endl;

	system("pause");
	system("CLS");

	cout << "Fibonacci" << endl << "Input number: "; // Fibonacci
	cin >> input;

	cout << fibonacci(input, 0, 1) << endl;

	system("pause");
	system("CLS");

	cout << "Check if the number is prime or not. Input number: "; // Prime Checker
	cin >> input;

	return 0;
}