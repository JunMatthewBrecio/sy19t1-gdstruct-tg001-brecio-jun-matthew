#include <iostream>
#include <string>
#include "time.h"

using namespace std;

void bubbleSortup(int integerArray[]) //Ascending
{
	cout << "Ascending: ";
	int placeTemp;
	for (int i = 0; i < 9; i++)
	{
		for (int x = 0; x < 9; x++)
		{
			if (integerArray[x] > integerArray[x + 1])
			{
				placeTemp = integerArray[x];
				integerArray[x] = integerArray[x + 1];
				integerArray[x + 1] = placeTemp;
			}
		}
	}
}

void bubbleSortdown(int integerArray[]) //Descending
{
	cout << "Descending: ";
	int placeTemp;
	for (int i = 0; i < 9; i++)
	{
		for (int x = 0; x < 9; x++)
		{
			if (integerArray[x] < integerArray[x + 1])
			{
				placeTemp = integerArray[x];
				integerArray[x] = integerArray[x + 1];
				integerArray[x + 1] = placeTemp;
			}
		}
	}
}

void printArray(int integerArray[])
{
	for (int x = 0; x < 10; x++)
	{
		cout << integerArray[x] << endl;
	}
}

void search(int searchChoice, int integerArray[])
{
	int counter = 0;
	for (int x = 0; x < 10; x++)
	{
		counter++;
		if (searchChoice == integerArray[x])
		{
			cout << "The number is in the list!" << endl;
		}
	}
	if (counter > 10)
	{
		cout << "The number is not in the list!" << endl;
	}
}

int main()
{
	int integerArray[10];
	int temp;
	int choice;
	int searchChoice;

	srand(time(NULL));

	for (int x = 0; x < 10; x++)
	{
		integerArray[x] = (rand() % 69) + 1;
		cout << integerArray[x] << endl;
	}

	cout << "Arrange in [1] Ascending or [2] Descending?" << endl;
	cin >> choice;

	if (choice == 1)
	{
		bubbleSortup(integerArray);
	}
	else if (choice == 2)
	{
		bubbleSortdown(integerArray);
	}

	cout << endl;

	printArray(integerArray);

	cout << "Search for number: ";
	cin >> searchChoice;

	search(searchChoice, integerArray);



	system("pause");
	return 0;
}
